$(document).ready(function() {
		highlightCells($("#limit").val());
});

function highlightCells(limit) {
	$.each($(".data"), function(idx, value) {
			var cell = $(this);
			if (parseInt(cell.html()) <= limit) {
				cell.addClass("danger");
			} else {
				cell.removeClass("danger");
			}
	});	
}

function limitChange() {
	var limit = $("#limit").val();
	$("#range").val(limit);
	highlightCells(limit);
}

function rangeChange() {
	var limit = $("#range").val();
	$("#limit").val(limit);
	highlightCells(limit);
}
